#pragma once
#pragma once

#ifndef __SUDOKU_H_INCLUDED__
#define __SUDOKU_H_INCLUDED__

#include <vector>

class Move {
public: 
	Move(int num, int r, int c) :num(num), r(r), c(c) {}

	int num;
	int r;
	int c;
};

class Board
{
public:
	Board();
	int* getBoard();
	void showBoard();
	void calcMoveList();
	void doMove(Move move);
	Move getMove(int idx);
	std::vector < Move > getMoveList();
	int getNum(int r, int c);
	size_t getMoveListN();

private:
	int grid[9][9];
	bool checkMoveValid(Move move);
	std::vector<Move> moveList;
	size_t moveListN;
};

#endif