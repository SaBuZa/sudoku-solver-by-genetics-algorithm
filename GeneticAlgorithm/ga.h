#pragma once
#pragma once

#ifndef __GA_H_INCLUDED__
#define __GA_H_INCLUDED__
#include <vector>

#include "sudoku.h"

class Gene
{
public:
	Gene();
	Gene(Board b);
	~Gene();
	static int geneNumber;
	Gene generateInitialGene();

	void calcFitness();
	void mutate();

	void setBirthdate();
	int getBirthdate() { return birthdate; }
	int getFitness() { return fitness; }
	Board getBoard() { return board; }
	
	bool operator<(Gene other) const {
		return fitness > other.fitness;
	}



private:
	int birthdate,fitness;
	Board board;
	std::vector<Move> moveSeq;
};

class GA {
public:
	GA();
	void generateInitialPopulation(int n);
	Gene generateChild();
	void generateChildPopulation(int  n);

	void executeCurrentGeneration();
	void executeGA();
	
	void showCurrentGeneration();
private:
	std::vector<Gene> geneVec;
	int N;
};
#endif
